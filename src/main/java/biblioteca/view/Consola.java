package biblioteca.view;


import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Consola {

	private BufferedReader bufferedReader;
	BibliotecaCtrl bibliotecaCtrl;
	
	public Consola(BibliotecaCtrl bibliotecaCtrl){
		this.bibliotecaCtrl = bibliotecaCtrl;
	}
	
	public void executa() throws IOException {
		
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		
		int option = -1;
		while(option!=0){
			
			switch(option){
				case 1:
					adauga();
					break;
				case 2:
					cautaCartiDupaAutor();
					break;
				case 3:
					afiseazaCartiOrdonateDinAnul();
					break;
				case 4:
					afiseazaToateCartile();
					break;
			}
		
			printMenu();
			String line;
			do{
				System.out.println("Introduceti un nr:");
				line= bufferedReader.readLine();
			}while(!line.matches("[0-4]"));
			option=Integer.parseInt(line);
		}
	}
	
	public void printMenu(){
		System.out.println("\n\n\n");
		System.out.println("Evidenta cartilor dintr-o biblioteca");
		System.out.println("     1. Adaugarea unei noi carti");
		System.out.println("     2. Cautarea cartilor scrise de un anumit autor");
		System.out.println("     3. Afisarea cartilor din biblioteca care au aparut intr-un anumit an, ordonate alfabetic dupa titlu si autori");
		System.out.println("     4. Afisarea toturor cartilor");
		System.out.println("     0. Exit");
	}
	
	public void adauga(){
		Carte carte = new Carte();
		try{
			System.out.println("\n\n\n");
			
			System.out.println("Titlu:");
			carte.setTitlu(bufferedReader.readLine());
			
			String line;
			do{
				System.out.println("An aparitie:");
				line= bufferedReader.readLine();
			}while(!line.matches("[10-9]+"));
			carte.setAnAparitie(Integer.parseInt(line));
			
			do{
				System.out.println("Nr. de referent:");
				line= bufferedReader.readLine();
			}while(!line.matches("[1-9]+"));
			int nrReferenti= Integer.parseInt(line);
			for(int i=1;i<=nrReferenti;i++){
				System.out.println("Autor "+i+": ");
				carte.adaugaReferent(bufferedReader.readLine());
			}

			System.out.println("Editura:");
			carte.setEditura(bufferedReader.readLine());
			
			do{
				System.out.println("Nr. de cuvinte cheie:");
				line= bufferedReader.readLine();
			}while(!line.matches("[1-9]+"));
			int nrCuv= Integer.parseInt(line);
			for(int i=1;i<=nrCuv;i++){
				System.out.println("Cuvant "+i+": ");
				carte.adaugaCuvantCheie(bufferedReader.readLine());
			}
			
			bibliotecaCtrl.adaugaCarte(carte);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void afiseazaToateCartile(){
		System.out.println("\n\n\n");
		try {
			for(Carte carte: bibliotecaCtrl.getCarti())
				System.out.println(carte);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void cautaCartiDupaAutor(){
	
		System.out.println("\n\n\n");
		System.out.println("Autor:");
		try {
			for(Carte carte: bibliotecaCtrl.cautaCarte(bufferedReader.readLine())){
				System.out.println(carte);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void afiseazaCartiOrdonateDinAnul(){
		System.out.println("\n\n\n");
		try{
			String line;
			do{
				System.out.println("An aparitie:");
				line= bufferedReader.readLine();
			}while(!line.matches("[10-9]+"));
			for(Carte carte: bibliotecaCtrl.getCartiOrdonateDinAnul(line)){
				System.out.println(carte);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
