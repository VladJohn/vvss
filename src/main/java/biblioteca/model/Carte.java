package biblioteca.model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Carte {
	
	private String titlu;
	private List<String> referenti;
	private Integer anAparitie;
	private String editura;
	private List<String> cuvinteCheie;
	
	public Carte(){
		titlu = "";
		referenti = new ArrayList<String>();
		anAparitie = 0;
		cuvinteCheie = new ArrayList<String>();
		editura = "";
	}

	public Carte(String titlu, List<String> referenti, Integer anAparitie, String editura, List<String> cuvinteCheie) {
		this.titlu = titlu;
		this.referenti = new ArrayList<String>(referenti);
		this.anAparitie = anAparitie;
		this.editura = editura;
		this.cuvinteCheie = new ArrayList<String>(cuvinteCheie);
	}

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public List<String> getReferenti() {
		return referenti;
	}

	public void setReferenti(List<String> referenti) {
		this.referenti = referenti;
	}

	public Integer getAnAparitie() {
		return anAparitie;
	}

	public void setAnAparitie(Integer anAparitie) {
		this.anAparitie = anAparitie;
	}

	public List<String> getCuvinteCheie() {
		return cuvinteCheie;
	}

	public void setCuvinteCheie(List<String> cuvinteCheie) {
		this.cuvinteCheie = cuvinteCheie;
	}
	

	public void deleteCuvantCheie(String cuvant){
			for(int i=0;i<cuvinteCheie.size();i++){
				if(cuvinteCheie.get(i).equals(cuvant)){
					cuvinteCheie.remove(i);
					return;
				}
			}
	}
	
	public void deleteReferent(String referent){
			for(int i=0;i<referenti.size();i++){
				if(referenti.get(i).equals(referent)){
					referenti.remove(i);
					return;
				}
			}
	}

	public String getEditura() {
		return editura;
	}

	public void setEditura(String editura) {
		this.editura = editura;
	}

	public void deleteTotiReferentii(){
		referenti.clear();
	}
	
	public void adaugaCuvantCheie(String cuvant){
		cuvinteCheie.add(cuvant);
	}
	
	public void adaugaReferent(String referent){
		referenti.add(referent);
	}
	
	public boolean cautaDupaCuvinteCheie(List<String> cuvinte){
		for(String c:cuvinteCheie){
			for(String cuv:cuvinte){
				if(c.equals(cuv))
					return true;
			}
		}
		return false;
	}
	 
	public boolean cautaDupaAutor(String autor){
		for(String a:referenti){
			if(a.contains(autor))
				return true;
		}
		return false;
	}
	
	@Override
	public String toString(){
		String referenti = "";
		String cuvinteCheie = "";
		
		for(int i = 0; i< this.referenti.size(); i++){
			if(i== this.referenti.size()-1)
				referenti+= this.referenti.get(i);
			else
				referenti+= this.referenti.get(i)+",";
		}
		
		for(int i = 0; i< this.cuvinteCheie.size(); i++){
			if(i== this.cuvinteCheie.size()-1)
				cuvinteCheie+= this.cuvinteCheie.get(i);
			else
				cuvinteCheie+= this.cuvinteCheie.get(i)+",";
		}
		
		return titlu+";"+referenti+";"+anAparitie+";"+editura+";"+cuvinteCheie;
	}
	
	public static Carte getCarteFromString(String data){
		Carte carte = new Carte();
		String []atribute = data.split(";");
		String []referenti = atribute[1].split(",");
		String []cuvCheie = atribute[4].split(",");
		
		carte.titlu=atribute[0];
		for(String s:referenti){
			carte.adaugaReferent(s);
		}
		carte.anAparitie = Integer.parseInt(atribute[2]);
		carte.editura = atribute[3];
		for(String s:cuvCheie){
			carte.adaugaCuvantCheie(s);
		}
		
		return carte;
	}
	
}
