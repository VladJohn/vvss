package WB;

import biblioteca.begin.Start;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class Tests_WB {

    private BibliotecaCtrl ctrl;
    private CartiRepo repo;
    private CartiRepo repo2;
    @Before
    public void setUp() throws Exception{
        repo = new CartiRepo();
        repo2 = new CartiRepo("inexistent.db");
        ctrl = new BibliotecaCtrl(repo);
    }
    @Test
    public void Req02_TC01() throws Exception {
        //valid
        ctrl = new BibliotecaCtrl(repo);
        String autor = "a";

        List<Carte> actual_result = ctrl.cautaCarte(autor);
        List<Carte> expected_result = new ArrayList<Carte>();
        List<String> referenti = new ArrayList<String>();
        List<String> cuvinte_cheie = new ArrayList<String>();

        referenti.add("Mihai Eminescu");
        referenti.add("Ion Caragiale");
        referenti.add("Ion Creanga");
        cuvinte_cheie.add("povesti");
        cuvinte_cheie.add("povestiri");
        Carte c = new Carte("Povesti", referenti, 1973, "Corint", cuvinte_cheie);
        expected_result.add(c);

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("Sadoveanu");
        cuvinte_cheie.add("poezii");
        Carte c2 = new Carte("Poezii", referenti, 1973, "Corint", cuvinte_cheie);
        expected_result.add(c2);

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("George Calinescu");
        cuvinte_cheie.add("enigma");
        cuvinte_cheie.add("otilia");
        Carte c3 = new Carte("Enigma Otiliei", referenti, 1948, "Litera", cuvinte_cheie);
        expected_result.add(c3);

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("Caragiale Ion");
        cuvinte_cheie.add("caragiale");
        cuvinte_cheie.add("carnaval");
        Carte c4 = new Carte("Dale carnavalului", referenti, 1948, "Litera", cuvinte_cheie);
        expected_result.add(c4);

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("Mateiu Caragiale");
        cuvinte_cheie.add("mateiu");
        cuvinte_cheie.add("crailor");
        Carte c5 = new Carte("Intampinarea crailor", referenti, 1948, "Litera", cuvinte_cheie);
        expected_result.add(c5);

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("Mihai Eminescu");
        referenti.add("Ion Caragiale");
        referenti.add("Ion Creanga");
        referenti.add("Tudor");
        cuvinte_cheie.add("povesti");
        cuvinte_cheie.add("povestiri");
        Carte c6 = new Carte("Povesti", referenti, 1990, "Corint", cuvinte_cheie);
        expected_result.add(c6);

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("Autor1");
        cuvinte_cheie.add("Cuvant1");
        Carte c7 = new Carte("Carte1", referenti, 2015, "Corint", cuvinte_cheie);
        expected_result.add(c7);

        for (int i = 0; i<actual_result.size(); i++) {
            assertEquals(expected_result.get(i).getTitlu(), actual_result.get(i).getTitlu());
            assertEquals(expected_result.get(i).getReferenti(), actual_result.get(i).getReferenti());
            assertEquals(expected_result.get(i).getAnAparitie(), actual_result.get(i).getAnAparitie());
            assertEquals(expected_result.get(i).getCuvinteCheie(), actual_result.get(i).getCuvinteCheie());
            assertEquals(expected_result.get(i).getEditura(), actual_result.get(i).getEditura());
        }
    }
    @Test
    public void Req02_TC03() throws Exception{

        //autorul nu exista

        ctrl = new BibliotecaCtrl(repo);
        String autor = "Arghezi";

        List<Carte> actual_result = ctrl.cautaCarte(autor);
        List<Carte> expected_result = new ArrayList<Carte>();

        assertEquals(expected_result,actual_result);
    }
    @Test
    public void Req02_TC04() throws Exception{

        //lista de carti e goala

        ctrl = new BibliotecaCtrl(repo2);
        String autor = "em";

        List<Carte> actual_result = ctrl.cautaCarte(autor);
        List<Carte> expected_result = new ArrayList<Carte>();

        assertEquals(expected_result,actual_result);
    }
}
