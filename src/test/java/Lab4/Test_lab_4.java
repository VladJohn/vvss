package Lab4;

import biblioteca.begin.Start;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class Test_lab_4 {
    private BibliotecaCtrl ctrl;
    private CartiRepo repo;
    private CartiRepo repo2;
    @Before
    public void setUp() throws Exception{
        repo = new CartiRepo();
        repo2 = new CartiRepo("inexistent.db");
        ctrl = new BibliotecaCtrl(repo);
    }

    @Test
    public void Req03_TC01() throws Exception {
        //valid
        ctrl = new BibliotecaCtrl(repo);
        String an = "2015";

        List<Carte> actual_result = ctrl.getCartiOrdonateDinAnul(an);
        List<Carte> expected_result = new ArrayList<Carte>();
        List<String> referenti = new ArrayList<String>();
        List<String> cuvinte_cheie = new ArrayList<String>();

        referenti.clear();
        cuvinte_cheie.clear();
        referenti.add("Autor1");
        cuvinte_cheie.add("Cuvant1");
        Carte c7 = new Carte("Carte1", referenti, 2015, "Corint", cuvinte_cheie);
        expected_result.add(c7);

        for (int i = 0; i<actual_result.size(); i++) {
            assertEquals(expected_result.get(i).getTitlu(), actual_result.get(i).getTitlu());
            assertEquals(expected_result.get(i).getReferenti(), actual_result.get(i).getReferenti());
            assertEquals(expected_result.get(i).getAnAparitie(), actual_result.get(i).getAnAparitie());
            assertEquals(expected_result.get(i).getCuvinteCheie(), actual_result.get(i).getCuvinteCheie());
            assertEquals(expected_result.get(i).getEditura(), actual_result.get(i).getEditura());
        }
    }

    @Test
    public void Req03_TC02() throws Exception {
        //invalid
        ctrl = new BibliotecaCtrl(repo);
        String an = "2018";

        List<Carte> actual_result = ctrl.getCartiOrdonateDinAnul(an);
        List<Carte> expected_result = new ArrayList<Carte>();

        assertEquals(expected_result.size(), actual_result.size());
    }
}
