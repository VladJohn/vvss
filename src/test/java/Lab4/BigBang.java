package Lab4;

import biblioteca.begin.Start;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BigBang {

    private BibliotecaCtrl ctrl;
    private CartiRepo repo3;
    @Before
    public void setUp() throws Exception{
        repo3 = new CartiRepo("lab4.db");
        ctrl = new BibliotecaCtrl(repo3);
    }

    @Test
    public void Test_A() throws Exception {
        //valid
        repo3.Clear_db();
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 1973;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);
            System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC1: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }

    @Test
    public void Test_B() throws Exception {
        //valid
        ctrl = new BibliotecaCtrl(repo3);
        String autor = "a";

        List<Carte> actual_result = ctrl.cautaCarte(autor);
        List<Carte> expected_result = new ArrayList<Carte>();
        List<String> referenti = new ArrayList<String>();
        List<String> cuvinte_cheie = new ArrayList<String>();

        referenti.add("autor");
        referenti.add("autoare");
        cuvinte_cheie.add("cuv");
        cuvinte_cheie.add("cuvant");
        Carte c = new Carte("Titlu", referenti, 1973, "editura", cuvinte_cheie);
        expected_result.add(c);

        for (int i = 0; i<actual_result.size(); i++) {
            assertEquals(expected_result.get(i).getTitlu(), actual_result.get(i).getTitlu());
            assertEquals(expected_result.get(i).getReferenti(), actual_result.get(i).getReferenti());
            assertEquals(expected_result.get(i).getAnAparitie(), actual_result.get(i).getAnAparitie());
            assertEquals(expected_result.get(i).getCuvinteCheie(), actual_result.get(i).getCuvinteCheie());
            assertEquals(expected_result.get(i).getEditura(), actual_result.get(i).getEditura());
        }
    }

    @Test
    public void Test_C() throws Exception {
        //valid
        ctrl = new BibliotecaCtrl(repo3);
        String an = "1973";

        List<Carte> actual_result = ctrl.getCartiOrdonateDinAnul(an);
        List<Carte> expected_result = new ArrayList<Carte>();
        List<String> referenti = new ArrayList<String>();
        List<String> cuvinte_cheie = new ArrayList<String>();

        referenti.add("autor");
        referenti.add("autoare");
        cuvinte_cheie.add("cuv");
        cuvinte_cheie.add("cuvant");
        Carte c = new Carte("Titlu", referenti, 1973, "editura", cuvinte_cheie);
        expected_result.add(c);

        for (int i = 0; i<actual_result.size(); i++) {
            assertEquals(expected_result.get(i).getTitlu(), actual_result.get(i).getTitlu());
            assertEquals(expected_result.get(i).getReferenti(), actual_result.get(i).getReferenti());
            assertEquals(expected_result.get(i).getAnAparitie(), actual_result.get(i).getAnAparitie());
            assertEquals(expected_result.get(i).getCuvinteCheie(), actual_result.get(i).getCuvinteCheie());
            assertEquals(expected_result.get(i).getEditura(), actual_result.get(i).getEditura());
        }
    }

    @Test
    public void Test_Int() throws Exception{
        //valid
        repo3.Clear_db();
        ctrl=new BibliotecaCtrl(repo3);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 1973;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);
            System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC1: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);

        String autor = "a";
        List<Carte> actual_result2 = ctrl.cautaCarte(autor);
        List<Carte> expected_result = new ArrayList<Carte>();
        List<String> referenti = new ArrayList<String>();
        List<String> cuvinte_cheie = new ArrayList<String>();

        referenti.add("autor");
        referenti.add("autoare");
        cuvinte_cheie.add("cuv");
        cuvinte_cheie.add("cuvant");
        Carte c2 = new Carte("Titlu", referenti, 1973, "editura", cuvinte_cheie);
        expected_result.add(c2);

        for (int i = 0; i<actual_result2.size(); i++) {
            assertEquals(expected_result.get(i).getTitlu(), actual_result2.get(i).getTitlu());
            assertEquals(expected_result.get(i).getReferenti(), actual_result2.get(i).getReferenti());
            assertEquals(expected_result.get(i).getAnAparitie(), actual_result2.get(i).getAnAparitie());
            assertEquals(expected_result.get(i).getCuvinteCheie(), actual_result2.get(i).getCuvinteCheie());
            assertEquals(expected_result.get(i).getEditura(), actual_result2.get(i).getEditura());
        }

        String anul = "1973";
        List<Carte> actual_result3 = ctrl.getCartiOrdonateDinAnul(anul);
        for (int i = 0; i<actual_result3.size(); i++) {
            assertEquals(expected_result.get(i).getTitlu(), actual_result3.get(i).getTitlu());
            assertEquals(expected_result.get(i).getReferenti(), actual_result3.get(i).getReferenti());
            assertEquals(expected_result.get(i).getAnAparitie(), actual_result3.get(i).getAnAparitie());
            assertEquals(expected_result.get(i).getCuvinteCheie(), actual_result3.get(i).getCuvinteCheie());
            assertEquals(expected_result.get(i).getEditura(), actual_result3.get(i).getEditura());
        }
    }
}
