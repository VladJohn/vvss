package biblioteca;

import biblioteca.begin.Start;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class Tests {
    private BibliotecaCtrl ctrl;
    private CartiRepo repo;
    @Before
    public void setUp() throws Exception{
        repo = new CartiRepo();
        ctrl = new BibliotecaCtrl(repo);
    }
    @Test
    public void TC1_ECP() throws Exception {
        //valid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);
            System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC1: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC2_ECP() throws Exception{
        //an invalid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu1";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        //String an = "abc";
        Integer an = 2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC2: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC3_ECP() throws Exception{
        //an invalid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = -2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch(Exception ex){
            assertEquals("An invalid", ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC4_ECP() throws Exception{
        //lista autori invalida
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        //listaRef.add(123);
        //listaRef.add(124);
        //listaRef.add(125);
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch(Exception ex){
            System.out.println("TC4: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC5_ECP() throws Exception{
        //lista cuvinte invalida
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        //listaCuv.add(123);
        //listaCuv.add(132);
        //listaCuv.add(122);
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC5: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC6_BVA() throws Exception{
        //valid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 1990;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC6: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC7_BVA() throws Exception{
        //valid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 0;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC7: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC8_BVA() throws Exception{
        //an invalid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        Integer an = 2010;
        //Integer an;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC8: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC9_BVA() throws Exception{
        //an invalid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        //String an = "anul";
        Integer an = 2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC9: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
    @Test
    public void TC10_BVA() throws Exception{
        //an invalid
        ctrl=new BibliotecaCtrl(repo);
        int initialSize = ctrl.getCarti().size();
        String titlu="Titlu";
        String editura="editura";
        ArrayList<String> listaRef = new ArrayList<String>();
        listaRef.add("autor");
        listaRef.add("autoare");
        ArrayList<String> listaCuv = new ArrayList<String>();
        listaCuv.add("cuv");
        listaCuv.add("cuvant");
        //Double an = 3.14;
        Integer an = 2010;
        Carte c = new Carte();
        c.setEditura(editura);
        c.setAnAparitie(an);
        c.setTitlu(titlu);
        c.setReferenti(listaRef);
        c.setCuvinteCheie(listaCuv);
        try{
            ctrl.adaugaCarte(c);System.out.println("Carte adaugata!");}
        catch (Exception ex){
            System.out.println("TC10: "+ex.getMessage());
        }
        int newSize = ctrl.getCarti().size();
        int actual_result = newSize;
        //int expected_size = initialSize;
        int expected_size = initialSize+1;
        assertEquals(expected_size,actual_result);
    }
}